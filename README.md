# obcs_tools

Tools needed to build the firmware of the OBC of OpenSpace 1.0 and the OBC of the OTB.

## Content

1) STM32Cube_FW_F4_V1.26.0: STM32CubeHAL libraries used to develop firmware with STM32 MCUs.
2) FreeRTOS-Plus_V20210400: FreeRTOS+ libraries. Have a TCP/IP stack and FAT support.
3) cJSON: library used to parse and build json messages.

## Usage
This repo dir must be located in the root dir of the firmware projects. For example:

```bash
	.
	├── obcs_bootloader
	│   ├── Boot
	│   └── Debug
	└── obcs_tools
	    ├── 0.STM32Cube_FW_F4_V1.26.0
	    └── 1.FreeRTOS-Plus_V20210400
	    └── cJSON	    
```

Be sure to clone the repository with the submodules. If you already cloned the repository and want to clone the submodules just run this command:
```
git submodule update --init --recursive
```
In the repository folder.